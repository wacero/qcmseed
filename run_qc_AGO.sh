#!/bin/bash

exec 1> >(logger -s -t $(basename $0)) 2>&1

yesterday=`date -u --date="1 day ago" +"%Y-%m-%d"`
today=`date -u  +"%Y-%m-%d"`

eval "$(/$HOME/anaconda3/bin/conda shell.bash hook)"
conda activate qcmseed

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $DIR

python ./qc_avail_gap_over.py ./configuracion_paral.txt $yesterday $today 1
