
import os,sys
from qcmseed.core import qc_data
from qcmseed.utils import qc_utils
from get_mseed_data import get_mseed 
from obspy.core import UTCDateTime
from datetime import timedelta, datetime

from influxdb import InfluxDBClient
from multiprocessing import Pool
import logging

for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)


def main():
    
    '''
    Read the configuration file 
    tsdb stands for Time Series DataBase like Influxdb
    '''
       
    exec_dir=os.path.realpath(os.path.dirname(__file__))
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', filename=os.path.join(exec_dir,"qc_mseed.log"),level=logging.INFO )
    is_error=False
    
    if len(sys.argv)==1:
        is_error=True
    
    else:
        try:
            logging.info("Start qc control")
            run_param=qc_utils.read_parameters(sys.argv[1])
            
        except Exception as e:
            logging.info("Error reading configuration in file: %s" %str(e))
            raise Exception("Error reading configuration in file: %s" %str(e))

        try:
            '''
            Read the configuration text file with information of data servers, database servers, and stations to process:
            '''
            mseed_server_id=run_param["MSEED_SERVER"]["id"]
            mseed_server_config_file=run_param["MSEED_SERVER"]['file_path']
            
            tsdb_server_id=run_param["TIMESERIES_DATABASE"]["id"]
            tsdb_config_file=run_param["TIMESERIES_DATABASE"]["file_path"]
            
            station_file=run_param["STATION_FILE"]['file_path']           
            
        except Exception as e:
            logging.error("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))

        try:
            '''
            Read JSON files with information of the data servers
            and get that info
            '''
            tsdb_param=qc_utils.read_config_file(tsdb_config_file)
            tsdb_name=tsdb_param[tsdb_server_id]['dbname']
            mseed_server_param=qc_utils.read_config_file(mseed_server_config_file)
            mseed_service_name=mseed_server_param[mseed_server_id]['name']
            stations=qc_utils.read_config_file(station_file)
            
                        
        except Exception as e:
            logging.error("Error reading configuration file: %s" %str(e))
            raise Exception("Error reading configuration file: %s" %str(e))

        '''Creates conexion with the servers and defines clients to connect to them.'''
        try:
            mseed_client = get_mseed.choose_service(mseed_server_param[mseed_server_id])
        
        except Exception as e:
            logging.error("Failed to create mseed client: %s" %str(e))
            raise Exception("Failed to create mseed client: %s" %str(e))
        
        try: 
            tsdb_client=InfluxDBClient(host=tsdb_param[tsdb_server_id]['ip'],port=tsdb_param[tsdb_server_id]['port'],
                                       username=tsdb_param[tsdb_server_id]['user'],password=tsdb_param[tsdb_server_id]['password'],
                                       database=tsdb_param[tsdb_server_id]['dbname'])
        except Exception as e:
            logging.error("Failed to create database client: %s" %str(e))
            raise Exception("Failed to create database client: %s" %str(e))

        
        '''
        Get dates to process
        '''
        try:
            date_start = UTCDateTime(datetime.strptime(sys.argv[2],"%Y-%m-%d"))
            date_end = UTCDateTime(datetime.strptime(sys.argv[3],"%Y-%m-%d"))
            
        except Exception as e:
            logging.error("Error getting dates: %s" %str(e))
            raise Exception("Error getting dates: %s" %str(e))
        
        '''
        Choose execution mode
        '''
        try:
            mode=int(sys.argv[4])
        except Exception as e:
            logging.error("Error getting mode: %s" %str(e))
            raise Exception("Error getting mode: %s" %str(e))



        '''
        For each day in the interval:
        get data
        correct that data
        sliced in five minutes 
        '''
        
        day_interval=86400
        slice_interval=600
        
        if mode == 0:
            for day in qc_utils.perdelta(date_start,date_end,timedelta(days=1)):
                dayUTC=UTCDateTime(day)
                for station_name,station in stations.items():
                    calculate_qc_params(station,mseed_service_name,mseed_client,dayUTC,day_interval,slice_interval,tsdb_client,tsdb_name)
                
        if mode ==1:
            pool=Pool(processes=14)
        
            for day in qc_utils.perdelta(date_start,date_end,timedelta(days=1)):
                dayUTC=UTCDateTime(day)
                results=[pool.apply_async(calculate_qc_params,args=(station,mseed_service_name,mseed_client,dayUTC,
                                                     day_interval,slice_interval,tsdb_client,tsdb_name),callback=callback_function) for station_name,station in stations.items()]
            
            for res in results:
                print(res.get())
                
            pool.close()
            #'''
            #pool.starmap(calculate_qc_params,[(station,mseed_service_name,mseed_client,dayUTC,
            #                                     day_interval,slice_interval,tsdb_client,tsdb_name) for station_name,station in stations.items()])
            

    if is_error:
        logging.info(f'Usage: python {sys.argv[0]} configuration_file.txt start_date end_date')
        print(f'Usage: python {sys.argv[0]} CONFIGURATION_FILE.txt start_date end_date ')    
        
def callback_function(res):
    logging.info("Result of multiprocess: %s" %res)
    

def calculate_qc_params(station,mseed_service_name,mseed_client,dayUTC,day_interval,slice_interval,tsdb_client,tsdb_name):
    
    print(station['cod'])
    stream=get_mseed.get_stream(mseed_service_name,mseed_client,
                                     station['net'],station['cod'],station['loc'][0],station['cha'][0],
                                     dayUTC,window_size=day_interval)
    
    #Revisar el sampling rate de todo el stream
    logging.info("check multiple sampling rates")
    if qc_data.check_sampling_rate(stream)== False:
        
        print("start sampling_rate2influxdb")
        qc_data.insert_sampling_rate2influx(tsdb_client,stream,tsdb_name,"sampling_rate")
        stream=qc_data.correct_sampling_rate(stream)
        
        print("nuevo stream")
        print(stream)
    
    #LLAMAR A LA FUNCION QUE REVISE OVERLAPS ANTES DE CHECK_STREAM
    #INSERTAR GAPS, OVERLAPS EN INFLUX 
    logging.info("Start gaps/overlap count:%s, %s" %(station['cod'],dayUTC))
    lista_gaps=stream.get_gaps()
    qc_data.insert_gaps2influx(tsdb_client,lista_gaps,tsdb_name,"gaps")
    
    
    corrected_stream=qc_data.check_stream(stream,station,dayUTC)
    #Conseguir la metadata de la estacion
    try:
        fdsn_client = qc_utils.get_fdsn_client()
        station_metadata = fdsn_client.get_stations(dayUTC,network=station['net'], station=station['cod'],
                                                    channel=station['cha'],level="response")
    except:
        pass
    #LLAMAR A FUNCION PDF
    #
    stream_sliced=qc_data.get_sliced_stream(corrected_stream,dayUTC,slice_interval)
    #CALL QC FUNCTIONS
    logging.info("Start availability count:%s, %s" %(station['cod'],dayUTC))
    availability_day=qc_data.qc_availability_day(stream_sliced)
    #print("Availb. result :%s" %availability_day)
    #SEND QC DATA to influx
    qc_data.insert_influx(tsdb_client,availability_day,tsdb_name,"availability")
    
    
main()



