This repository has code to calculate quality control parameters of a SeisComP MSEED archive

## Installation
Clone this repository with `https://gitlab.com/wacero/qcmseed.git`

Dependencies
 - Python 3
 - Obspy 1.1 
 - Influxdb 5.3

## Usage

Run as Python script

```bash
$ python ./qc_avail_gap_over.py ./configuracion_paral.txt 2020-01-01 2020-02-01 1

```
Run as crontab job

```bash
17 19 * * * $HOME/qcmseed/run_qc_AGO.sh
 ```
