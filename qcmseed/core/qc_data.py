'''
Created on Jun 7, 2017

@author: wacero
'''
from ..utils import qc_utils

from obspy import Stream, Trace
import numpy as np
import matplotlib.pyplot as plt
import json
import logging

SECONDS_DAY=86400
SAMPLING_RATE=100


def insert_influx(client_influxdb,data_list,dbname,measure):
    
    """
    TODO
    MOdify to insert other measures like gaps and database name. 
    SE PUEDE UNIFICAR NO USANDO DICCIONARIOS SOLO ARREGLOS. 
    """
    
    data_line_list=[]
    for data in data_list:
        data_line_list.append("quality_control,station_channel=%s availability=%s %s" %(data['station_channel'],data['qc_key']['qc_availability'],
                                                                             int(data['time'].timestamp)))
    #print(data_line_list)
    res=client_influxdb.write_points(data_line_list,time_precision="s", database=dbname,protocol="line")
    print(res)
         

def insert_gaps2influx(client_influxdb,data_list,dbname,measure):
    
    data_gap_list=[]
    data_overlap_list=[]
    
    data_gap_aux_list=[]
    data_overlap_aux_list=[]
    
    
    for data in data_list:
        station=data[1]
        channel=data[3]
        gap_starttime=data[4]
        gap_duration=data[6]
        
        if gap_duration >0:
            data_gap_list.append("quality_control,station_channel=%s_%s gap=%s %s " %(station,channel,gap_duration, int(gap_starttime.timestamp)))
            #data_gap_aux_list.append("quality_control,station_channel=%s_%s gap_aux=%s %s " %(station,channel,1, int(gap_starttime.timestamp)))
        else:
            data_overlap_list.append("quality_control,station_channel=%s_%s overlap=%s %s " %(station,channel,gap_duration, int(gap_starttime.timestamp)))
            #data_overlap_aux_list.append("quality_control,station_channel=%s_%s overlap_aux=%s %s " %(station,channel,1, int(gap_starttime.timestamp)))
        
    client_influxdb.write_points(data_gap_list,time_precision="s",database=dbname,protocol="line")
    client_influxdb.write_points(data_overlap_list,time_precision="s",database=dbname,protocol="line")
    #client_influxdb.write_points(data_gap_aux_list,time_precision="s",database=dbname,protocol="line")
    #client_influxdb.write_points(data_overlap_aux_list,time_precision="s",database=dbname,protocol="line")
    
def insert_sampling_rate2influx(client_influxdb,stream,dbname,measure):
    
    data_list=[]
    
    for trace in stream:
        #print(trace.stats)
        data_list.append("quality_control,station_channel=%s_%s sampling_rate=%s %s" %(trace.stats['station'],trace.stats['channel'],
                                                                                        trace.stats['sampling_rate'], trace.stats['starttime'].ns))
    
    client_influxdb.write_points(data_list,time_precision="n",database=dbname,protocol="line")





def check_stream(stream,station_info,dayUTC):
    """
    Check stream problems, return a stream even if the original is emtpy
    If there is no stream create an empty one, if the stream has gaps, fill the gaps with zeros.
    This is necessary because if there is no data for a whole day this should be registered also. 
    Code like obspy-scan checks the difference between starttime and endtime and finds
    the duration of gaps in seconds. 
    """
    if len(stream)==0:
        logging.info("Stream empty: %s" %station_info)
        return create_empty_stream(station_info,dayUTC,SAMPLING_RATE*SECONDS_DAY)
    else:
        ##TODO
        ##Check just one snlc as in MUSTANG 
        stream.merge(fill_value=0)
        return stream


def check_sampling_rate(stream):
    
    if len(stream)>1:
        sampling_rate_list=[]
        for trace in stream:
            sampling_rate_list.append(trace.stats['sampling_rate'])
    
        sampling_rate_set=set(sampling_rate_list)
        
        if len(sampling_rate_set)>1:
            logging.warning("Multiple sampling rates detected:%s" %stream)
            print("Multiple sampling rate detected:%s" %stream)
            return False
        
        else:
            return True
        
    else:
        return True

def correct_sampling_rate(stream):
    
    

    stream=stream.sort(keys=['npts'])
    
    prefered_sampling_rate=(stream[-1].stats['sampling_rate'])    
        
    for trace in stream:
        if trace.stats['sampling_rate'] != prefered_sampling_rate:
            stream.traces.remove(trace)
    
    return stream


def create_empty_stream(station,dayUTC,data_length):
    """
    Create a stream filled with zeros 
    """
    data=np.zeros(data_length)
    trace=Trace()
    trace.stats.starttime=dayUTC
    trace.stats.network=station["net"]
    trace.stats.station=station["cod"]
    trace.stats.location=station["loc"][0]
    trace.stats.channel=station["cha"][0]
    trace.stats.sampling_rate=SAMPLING_RATE   
    trace.data=data
    
    return Stream(traces=[trace])
    

def get_sliced_stream(stream, start_time,slice_interval):
    """
    Receives a day stream duration and returns a list of sliced streams, 
    If the sliced streams doesn't exist, create a stream with zeros.  
    """
    
    stream_sliced=[0]*int(SECONDS_DAY/slice_interval)
    
    for x in range(0,int(SECONDS_DAY/slice_interval)):
        slice_start=start_time + (slice_interval*x)
        slice_end = start_time + (slice_interval*(x+1))
        stream_temp=stream.slice(slice_start,slice_end)
        '''
        Create an empty stream if necessary
        '''
        if len(stream_temp)==0:
            station={"net":stream[0].stats.network, "cod":stream[0].stats.station,"loc":[stream[0].stats.location],"cha":[stream[0].stats.channel]}
            stream_temp=create_empty_stream(station, slice_start, slice_interval)
        
        stream_sliced[x]= stream_temp
    
    return stream_sliced


def qc_availability_day(stream_list):
    """
    Receives a stream list and returns a list of dictionary with availability values
    If the sliced stream maximum value is different from 0, availability is 1
    """
    
    qc_availability_list=[]
    
    for stream in stream_list:
        trace=stream[0]
        qc_availab={"station_channel": "%s_%s" %(trace.stats['station'],trace.stats['channel']),
                    "time":trace.stats.starttime,
                    "qc_key":{}}
        
        if trace.max() !=0:
            
            #print("Data exist")
            qc_availab["qc_key"].update({'qc_availability':1})
            
        else:
            #print("No data")
            qc_availab["qc_key"].update({'qc_availability':0})

        qc_availability_list.append(qc_availab)
        
    return qc_availability_list









    
def qc_availability(stations,dUTC, service, srvConn, DBConn, interval):
    """
    Get stream of "interval" duration and check if max value different from 0
    """
    logging.debug("Start qc_availability()")
    for sta in stations.itervalues():
        dict_insert={'cod':sta['cod'],'dutc':dUTC,'cha_k':{}}
        for loc in sta['loc']:
            for cha in sta['cha']:
                dat=qc_utils.get_stream(service,srvConn,sta['net'],sta['cod'],loc,cha,dUTC,interval)  
                
                if dat!= None and len(dat)>0:
                    logging.info("Get max value of data for: %s.%s.%s" % (sta['net'], sta['cod'], dUTC))
                    if dat.max() != 0:
                        print("INSERTAR 100")
                        dict_insert['cha_k'].update({'availability':1})
                        print(dict_insert)
                        zabbix_sender(dict_insert)
                    else:
                        print("INSERTAR 0")
                        dict_insert['cha_k'].update({'availability':0})
                        zabbix_sender(dict_insert)
                else:
                    print("INSERTAR 0")
                    dict_insert['cha_k'].update({'availability':0})
                    zabbix_sender(dict_insert)

def qc_availability_fill(stations,dUTC, service, srvConn, influx_client,period, interval):
        
    logging.info("Start qc_availability_fill()")
    #for sta in stations.itervalues():
    for key,sta in stations.items():
        for loc in sta['loc']:
            for cha in sta['cha']:
                dat=qc_utils.get_stream(service,srvConn,sta['net'],sta['cod'],loc,cha,dUTC,period)  
                
                if dat!= None and len(dat)>0:
                    logging.info("Slice data in minutes for: %s.%s.%s" % (sta['net'], sta['cod'], dUTC))
                    slice_stream(dat,dUTC,interval,influx_client)

                else:
                    logging.info("Filling with 0s")
                    fill_zeros(sta['cod'],dUTC,interval,influx_client)

def fill_zeros(cod_station,start_time,interval,influx_client):
    '''
    Send 0 to zabbix availability item for a whole day
    '''    
    for x in range(0,int(86400/interval)):
        
        slice_start_time=start_time+(interval*x)        
        dict_insert={'cod':cod_station,'dutc':slice_start_time,'cha_k':{'qc_availability':0}}
        print(dict_insert)
        #zabbix_sender(dict_insert)
        #insert_influx(influx_client,dict_insert)



def slice_stream(stream_station,start_time, interval,influx_client):
    
    for x in range(0,int(86400/interval)):
        
        slice_start_time=start_time+(interval*x)
        slice_end_time=start_time+(interval*(x+1))
        dict_insert={'cod':"%s_%s" %(stream_station[0].stats['station'],stream_station[0].stats['channel']),'dutc':slice_start_time,'cha_k':{}}
        
        temporal_stream=stream_station.slice(slice_start_time,slice_end_time)
        
        
        if len(temporal_stream)!= 0:
            logging.debug("Data exist, find max")
            if temporal_stream.max()!= 0:
                dict_insert['cha_k'].update({'qc_availability':1})
                #print(dict_insert)
                #insert_influx(influx_client,dict_insert)
                #zabbix_sender(dict_insert)
            else:
                dict_insert['cha_k'].update({'qc_availability':0})
                #insert_influx(influx_client,dict_insert)
                #zabbix_sender(dict_insert)
        else:
            dict_insert['cha_k'].update({'qc_availability':0})
            #insert_influx(influx_client,dict_insert)
            #zabbix_sender(dict_insert)


               

def qc_insert(stations, dUTC, service, srvConn, DBConn, interval):
    
    logging.debug("starting qc_insert()")
    
    for sta in stations.itervalues():
        
        dict_insert = {'cod':sta['cod'], 'dutc':dUTC, 'cha_k':{}}
        gaps_count = 0
        overlap_count = 0
        gaps_duration = 0
        overlap_duration = 0
               
        for loc in sta['loc']:
            for cha in sta['cha']:
                
                dat = getStream(service, srvConn, sta['net'], sta['cod'], loc, cha, dUTC, interval)
                if dat != None and len(dat) > 0:                    
                    logging.info("counting gaps, overlaps for: %s.%s.%s" % (sta['net'], sta['cod'], dUTC))

                    gaps_result = dat.get_gaps()
                    for g in gaps_result:
                        if g[6] > 0:
                            # #Insert g in BD as gaps
                            gaps_count += 1
                            gaps_duration += g[6]
                        else:
                            # #Insert o in BD as overlaps
                            overlap_count += 1
                            overlap_duration += g[6] * (-1)
                    print('%s: %s,%s,%s,%s,%s,%s') % (dUTC.strftime("%Y.%m.%d"), sta['cod'], cha, gaps_count, int(gaps_duration), overlap_count, int(overlap_duration))
                    logging.info("counting result: %s,%s,%s,%s,%s,%s " % (sta['cod'], cha, gaps_count, int(gaps_duration), overlap_count, int(overlap_duration)))
                            
                    dict_insert['cha_k'].update({'gaps_count':gaps_count})
                    dict_insert['cha_k'].update({'gaps_duration':int(gaps_duration)})
                    dict_insert['cha_k'].update({'overlap_count':overlap_count})
                    dict_insert['cha_k'].update({'overlap_duration':int(overlap_duration)})

                    # #Llamar a zabbix_sender                   
                    zabbix_sender(dict_insert)
                
                    return (sta['cod'], gaps_count, int(gaps_duration), overlap_count, int(overlap_duration))
                                       
                else:
                    logging.info("No data for: %s %s. Filling with -1" % (sta['net'], sta['cod']))
                    dict_insert['cha_k'].update({'gaps_count':0})
                    dict_insert['cha_k'].update({'gaps_duration':0})
                    dict_insert['cha_k'].update({'overlap_count':0})
                    dict_insert['cha_k'].update({'overlap_duration':0})
                    zabbix_sender(dict_insert)
                    print('%s: %s,%s,%s,%s,%s,%s') % (dUTC.strftime("%Y.%m.%d"), sta['cod'], cha, gaps_count, int(gaps_duration), overlap_count, int(overlap_duration))
                    
                    ##Llamar a BD
                    return (sta['cod'], gaps_count, int(gaps_duration), overlap_count, int(overlap_duration))
                    #return -1
                       
            logging.debug(dict_insert)
                            
    # qc_plot(stations_g,gaps_durationurat,dUTC,'gaps')
    # qc_plot(stations_o,over_durat,dUTC,'overlaps')   

def check_gaps_value(stats, dUTC):
    stat_g = []
    stat_o = []
    gaps_count = []
    gaps_duration = []
    overlap_count = []
    overlap_duration = []  

    stations = sorted(stats, reverse=True)
    for qc_r in stations:
        if qc_r != -1:
            if qc_r[1] > 100 or qc_r[2] > 1000:
                stat_g.append(qc_r[0])
                gaps_count.append(qc_r[1])
                gaps_duration.append(qc_r[2])
            if qc_r[3] > 100 or qc_r[4] > 1000:
                stat_o.append(qc_r[0])
                overlap_count.append(qc_r[3])
                overlap_duration.append(qc_r[4])
       
    qc_plot(stat_g, gaps_duration, dUTC, 'gaps')
    qc_plot(stat_o, overlap_duration, dUTC, 'overlaps')


           

            
            
            
