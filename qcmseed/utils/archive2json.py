
import os,sys
import fnmatch,re
import argparse 

parser = argparse.ArgumentParser()
parser.add_argument("sc3_archive_path")
parser.add_argument("year",type=int)
parser.add_argument("net")

args = parser.parse_args()

sc3_archive_path=args.sc3_archive_path
year=args.year
net=args.net


archive_path="%s/%s/%s" %(sc3_archive_path,year,net)

station_list=[]
i=0
for station in os.listdir(archive_path):
    
    if os.path.isdir("%s/%s" %(archive_path,station)):
        
        for channel in os.listdir("%s/%s" %(archive_path,station)):
            if re.search(channel,"HHZ.D") or re.search(channel,"HNZ.D") or \
            re.search(channel,"BHZ.D") or re.search(channel,"SHZ.D") or re.search(channel,"BLZ.D") or \
            re.search(channel,"BDF.D"):
                if re.search(channel,"BDF.D"):
                    for root,dirs,files in os.walk("%s/%s/%s" %(archive_path,station,channel)):
                        for file in files:
                            if fnmatch.fnmatch(file,"*01.BDF.D*"):
                                station_list.append('"%s_%s":{"net":"%s","cod":"%s","loc":["01"],"cha":["%s"]},' %(station,i,net,station,channel[:3]))
                                break
                            
                            elif fnmatch.fnmatch(file,"*..BDF.D*"):
                                station_list.append('"%s_%s":{"net":"%s","cod":"%s","loc":[""],"cha":["%s"]},' %(station,i,net,station,channel[:3])) 
                                break
                else:
                    station_list.append('"%s_%s":{"net":"%s","cod":"%s","loc":[""],"cha":["%s"]},' %(station,i,net,station,channel[:3]))
                i+=1
    #print(dir_names)
station_list.sort()


for station in station_list:
    print(station)




