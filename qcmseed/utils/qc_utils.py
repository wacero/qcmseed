import json
import logging
import configparser

from obspy.clients.arclink import Client as clientArclink
from obspy.clients.seedlink import Client as clientSeedlink
from obspy.clients.filesystem.sds import Client as clientArchive

def read_config_file(json_file_path):
    '''
    Read a json file and returns a python dict
    
    :param string json_file_path: path to the configuration file
    :returns: dict: dictionary with the server parameters
    '''
    
    json_file=check_file(json_file_path)
    with open(json_file) as json_data:
        return json.load(json_data)
    
def read_parameters(file_path):
    """
    Read a configuration text file
    
    :param string file_path: path to configuration text file
    :returns: dict: dict of a parser object
    """
    parameter_file=check_file(file_path)
    parser=configparser.ConfigParser()
    parser.read(parameter_file)    
    return parser._sections



def check_file(file_path):
    '''
    Check if the file exists
    
    :param string file_path: path to file to check
    :return: file_path
    :raises Exception e: General exception if file doesn't exist. 
    '''
    try:
        
        with open(file_path):
            return file_path

    except Exception as e:
        logging.error("Error in check_file(%s). Error: %s " %(file_path,str(e)))
        raise Exception("Error in check_file(%s). Error: %s " %(file_path,str(e)))



def perdelta(start, end, delta):
    '''
    Yields a day date between two datetimes 
    '''
    current = start
    while current < end:
        yield current
        current += delta



            
def get_fdsn_client(fdsn_server,fdsn_port):
    
    try:
        return Client("http://%s:%s" %(fdsn_server,fdsn_port))
    except Exception as e:
        logging.error("Error in get_fdsn_client: %s" %str(e))
            
            







